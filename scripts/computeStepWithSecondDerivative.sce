// Copyright (C) 2008-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Consider finite difference formulas and try to get the 
// most accurate finite difference step h.
// There are two ways:
// * get a more accurate estimate of the rounding error,
// * get a more accurate estimate of the second derivative.
// Here, we estimate the second derivative.
//

// Computes the approximate step for the forward F.D. formula.
// Uses the unscaled step.
function y = step_approxUnscaled ( x )
    y = sqrt(%eps)
endfunction

// Computes the approximate step for the forward F.D. formula.
// Uses the scaled step.
function y = step_approxScaled ( x )
    y = sqrt(%eps) * abs(x)
endfunction

// Computes the exact step h for 
// the given function f at point x.
// Considers the forward F.D. formula.
function y = step_exact ( f , fpp , x )
    y = sqrt( 2 * %eps * abs(f(x)) / abs(fpp(x)))
endfunction

// Computes the first derivative with a forward F.D.
function y = forwardFD ( f , x , h )
    y = ( f(x+h) - f(x))/h
endfunction

// Computes the second derivative with a centered F.D.
// The approximate optimal step is h=(12*%eps)^(1/4)
function y = centeredSecond ( f , x , h )
    y = ( f(x+h) - 2*f(x) + f(x-h))/(h^2)
endfunction

// Computes the approximately exact step h for 
// the given function f at point x.
// Considers the forward F.D. formula.
// Uses an approximate second derivative with 
// approximately optimal step.
function h = step_second ( f , x )
    hpp=(12*%eps)^(1/4) * abs(x)
    fpp = centeredSecond ( f , x , hpp )
    if ( fpp == 0 ) then
        fpp = 1
    end
    h = sqrt( 2 * %eps * abs(f(x)) / abs(fpp))
endfunction

// Computes the condition number of the function
function c = conditionnumber(x,f,fp)
    c = abs(x*fp(x)/f(x))
endfunction

// Compare the unscaled steps, the scaled step and the exact step 
// with the plot of the Relative Error.
function compareSteps(f , fp, fpp , x )
    mprintf ( "==================\n")
    mprintf ( "x = %s\n", string(x))
    mprintf ( "f(x) = %s\n", string(f(x)))
    c = conditionnumber(x,f,fp)
    mprintf ( "C(x) = %s\n", string(c))
    //
    h1 = step_approxUnscaled ( x );
    mprintf ( "Step Unscaled = %s\n", string(h1))
    //
    hS = step_approxScaled ( x );
    mprintf ( "Step Scaled = %s\n", string(hS))
    //
    h2 = step_exact ( f , fpp , x );
    mprintf ( "Step Exact = %s\n", string(h2))
    //
    hSec = step_second ( f , x );
    mprintf ( "Step Sec. = %s\n", string(hSec))
endfunction

///////////////////////////////////////////////////////////
//
// Applies the theory on the sqrt function
//

// The square root function
function y = mysqrt ( x )
    y = sqrt(x)
endfunction

// The derivative of the square root function.
function y = mydsqrt ( x )
    y = 0.5 * x^(-0.5)
endfunction

// The second derivative of the square root function.
function y = myddsqrt ( x )
    y = -0.25 * x^(-1.5)
endfunction

// Try in x = 1
// Plot the Relative Error + step
// All steps are ok.
x = 1.0;
compareSteps(mysqrt , mydsqrt, myddsqrt , x );


// Try in x = 1.e10
// Plot the Relative Error + step
// The unscaled step is very bad (too small).
// The second step is excellent.
x = 1.e10;
compareSteps(mysqrt , mydsqrt, myddsqrt , x );

// Try in x = 1.e-10
// Plot the Relative Error + step
// The unscaled step is very bad (too large).
// The second step is excellent.
x = 1.e-10;
compareSteps(mysqrt , mydsqrt, myddsqrt , x );

///////////////////////////////////////////////////////////
//
// Applies the theory on the power^20 function
//

// The power^20 function
function y = mypower20 ( x )
    y = x^20
endfunction

// The derivative of the power20 function.
function y = mypower20p ( x )
    y = 20*x^19
endfunction

// The second derivative of the power20 function.
function y = mypower20pp ( x )
    y = 20*19*x^18
endfunction



// Try in x = 1.e10
// Plot the Relative Error + step
// The unscaled step is very bad.
// The second step is excellent.
x = 1.e10;
compareSteps(mypower20 , mypower20p, mypower20pp , x );


// Try in x = 1.e-10
// Plot the Relative Error + step
// The unscaled step is very bad.
// The second step is excellent.
x = 1.e-10;
compareSteps(mypower20 , mypower20p, mypower20pp , x );

///////////////////////////////////////////////////////////
//
// Applies the theory on the exp function
//

// The exp function
function y = myexp ( x )
    y = exp(x)
endfunction


// Try in x = 1
// Plot the Relative Error + step
x = 1.0;
compareSteps(myexp , myexp, myexp , x );


// Try in x = 1.e-10
// Plot the Relative Error + step
// The scaled step is very bad.
// The second step is excellent.
x = 1.e-10;
compareSteps(myexp , myexp, myexp , x );

///////////////////////////////////////////////////////////
//
// Applies the theory on the exp(a*x) function
//

// The exp function
function y = myexpA ( x )
    global MYA
    a = MYA
    y = exp(a * x)
endfunction

function y = myexpAp ( x )
    global MYA
    a = MYA
    y = a*exp(a * x)
endfunction

function y = myexpApp ( x )
    global MYA
    a = MYA
    y = a^2*exp(a * x)
endfunction


// In this case:
// * the unscaled step is much too large,
// * the scaled step is much too small.
global MYA
MYA = 1.e10
x = 1.e-20;
compareSteps(myexpA , myexpAp, myexpApp , x );

