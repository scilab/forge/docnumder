// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Check that order 4 formula is associated to correct step length.
function fp = myfprime(f,x,h)
  fp = (f(x+h) - f(x))/h;
endfunction
function y = myfunction (x)
  y = x*x;
endfunction
x = 1.0;
h = 1.0;
for i=1:20
  h=h/10.0;
  fp = derivative(myfunction,x,h,order=4);
  e = abs(fp-2.0)/2.0;
  mprintf("Naive f''=%e, h=%e, error=%e\n", fp,h,e);
end

