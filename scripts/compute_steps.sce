// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Compute optimal steps and optimal errors for various formulas.

// Forward Order 1 Formula for First Derivative
h = sqrt(%eps)
E = 2 * sqrt(%eps)
mprintf("h=%e",h)
mprintf("E=%e",E)

// 2 points Centered Order 2 Formula for First Derivative
h = %eps^(1/3)
E = (3/2) * %eps^(2/3)
mprintf("h=%e",h)
mprintf("E=%e",E)

// 4 points Centered Order 4 Formula for First Derivative
h = %eps^(1/5)
E = (5/4) * %eps^(4/5)
mprintf("h=%e",h)
mprintf("E=%e",E)

// 3 points Centered Order 2 Formula for Second Derivative
h = %eps^(1/4)
E = 2 * %eps^(1/2)
mprintf("h=%e",h)
mprintf("E=%e",E)

