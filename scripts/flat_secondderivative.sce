// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Present an example where a flat second derivative generates very
// inaccurate first numerical derivative.
function y = myfunction ( x )
  global mytest_n
  n = mytest_n
  y = x^(2/n)
endfunction
function fp = fprime ( x )
  global mytest_n
  n = mytest_n
  fp = (2/n) * x^(2/n-1)
endfunction
function fp = fsecond ( x )
  global mytest_n
  n = mytest_n
  fp = (2/n) * (2/n-1) * x^(2/n-2)
endfunction
function fp = fthird ( x )
  global mytest_n
  n = mytest_n
  fp = (2/n) * (2/n-1) * (2/n-2) * x^(2/n-3)
endfunction
function fp = ffourth ( x )
  global mytest_n
  n = mytest_n
  fp = (2/n) * (2/n-1) * (2/n-2) * (2/n-3) * x^(2/n-4)
endfunction
global mytest_n
npts = 1000;
// Draw f for various n
for mytest_n = [2 5 20]
  x = linspace ( 0 , 2 , npts );
  y = zeros ( 1 , npts );
  for i=1:npts
    y(i) = myfunction ( x ( i ) );
  end
  plot(x,y)
end
// Compute the derivative for various n at x=1
// We see increasing error :
// n = 1, Error=7.450581e-009
// n = 10, Error=1.490116e-008
// n = 100, Error=2.086163e-007
// n = 1000, Error=2.026558e-006
// n = 100000, Error=1.320839e-004
// n = 1410065408, Error=1.000000e+000
x = 1
for mytest_n = [1.e0 1.e1 1.e2 1.e3  1.e5 1.e10]
  expected = fprime ( x );
  computed = derivative ( myfunction , x , order = 1 );
  mprintf("n = %d, Error=%e\n",mytest_n, abs(computed-expected)/expected);
end
// Analyse the problem for n=1.e10
mytest_n = 1.e10
expected = fprime ( x );
computed = derivative ( myfunction , x , order = 1 , verbose = 1 );
mprintf("n = %d, Error=%e\n",mytest_n, abs(computed-expected)/expected);
// We see that the step is small (while the function is very flat)
// h = 1.490116e-008
// Compute a larger step for an order 1 formula
h = sqrt(2 * %eps * abs(myfunction ( x )) / abs(fsecond ( x )))
computed = derivative ( myfunction , x , h = h , order = 1 , verbose = 1 );
mprintf("n = %d, Error=%e\n",mytest_n, abs(computed-expected)/expected);
// We see that 
// * the step is larger :
//   h = 1.490116e-003
// * the error is reduced
//   n = 1410065408, Error=8.771421e-004
// Now compute a step with an order 2 formula
h = (3 * %eps * abs(myfunction ( x )) / abs(fthird ( x )))^(1/3)
computed = derivative ( myfunction , x , h = h , order = 2 , verbose = 1 );
mprintf("n = %d, Error=%e\n",mytest_n, abs(computed-expected)/expected);
// We see that :
// * the step is even larger :
//   h = 1.185315e-002
// * the error is reduced further
//   n = 1410065408, Error=5.904663e-005
// Try order 4 step and formula
h = (15 * %eps * abs(myfunction ( x )) / abs(ffourth ( x )) / 2)^(1/5)
computed = derivative ( myfunction , x , h = h , order = 4 , verbose = 1 );
mprintf("n = %d, Error=%e\n",mytest_n, abs(computed-expected)/expected);
// h = 6.736961e-002
// n = 1410065408, Error=1.074869e-005
// The error does not decrease much.



