// Copyright (C) 2010 - Michael Baudin

function f = quadf ( x )
    f = x(1)^2 + x(2)^2
    plot(x(1)-1,x(2)-1,"bo")
endfunction

function J = quad_Jf ( x )
    J(1) = 2 * x(1)
    J(2) = 2 * x(2)
endfunction

function H = quad_Hf ( x )
    H(1,1:2) = [ 2 * x(1) , 0]
    H(2,1:2) = [ 0        , 2 * x(2)]
endfunction


// Check derivatives of F
x = [1 1]'
[J1 , H1]  = derivative(quadf,x,H_form="blockmat" )
J2 = quad_Jf ( x )'
H2 = quad_Hf ( x )'
norm(J1-J2)
norm(H1-H2)

function updateBounds(h)
    hc = h.children
    hc.axes_visible=["off" "off","off"];
    hc.data_bounds(1,:) = -hc.data_bounds(2,:);
    hc.data_bounds = 1.1*hc.data_bounds;
	for i = 1 : size(hc.children,"*")
		hc.children(i).children.mark_background=2
	end
endfunction

// See pattern for Jacobian
h = scf();
J1  = derivative(quadf,x,order=1 );
updateBounds(h);
h = scf();
J1  = derivative(quadf,x,order=2 );
updateBounds(h);
h = scf();
J1  = derivative(quadf,x,order=4 );
updateBounds(h);

// See pattern for Hessian for order 2
h = scf();
[J1 , H1]  = derivative(quadf,x,order=1 );
updateBounds(h);
h = scf();
[J1 , H1]  = derivative(quadf,x,order=2 );
updateBounds(h);
h = scf();
[J1 , H1]  = derivative(quadf,x,order=4 );
updateBounds(h);

