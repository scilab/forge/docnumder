// Copyright (C) 2008-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Consider the forward formula to compute the first derivative
// of a scalar function of one parameter.
//
// We use the forward finite difference formula.
// The unscaled step is sqrt(eps), 
// the scaled step is sqrt(eps)*abs(x).
//
// Some significant experiments are:
// * f(x) = sqrt(x), x = 1
//   All steps are ok.
// * f(x) = sqrt(x), x = 1.e10
//   The unscaled step is very bad (too small).
// * f(x) = sqrt(x), x = 1.e-10
//   The unscaled step is very bad (too large).
// * f(x) = exp(x), x = 1.e-10
//   Plot the Relative Error + step
//   The scaled step is very bad (too small).
//   The cause is the scaling, which is bad in this case.
//
// The worst case of all is f(x)=exp(1.e10*x) for x=1.e-20.
// In this case:
// * the unscaled step is much too large,
// * the scaled step is much too small.
// To fix this case:
// * get a more accurate estimate of the rounding error,
// * get a more accurate estimate of the second derivative.
//

// Computes the approximate step for the forward F.D. formula.
// Uses the unscaled step.
function y = step_approximate ( x )
    y = sqrt(%eps)
endfunction

// Computes the approximate step for the forward F.D. formula.
// Uses the scaled step.
function y = step_approxScaled ( x )
    y = sqrt(%eps) * abs(x)
endfunction

// Computes the approximately exact step h for 
// the given function f at point x.
// Considers the forward F.D. formula.
function y = step_exact ( f , fpp , x )
    y = sqrt( 2 * %eps * abs(f(x)) / abs(fpp(x)))
endfunction

// Computes the first derivative with a forward F.D.
function y = forwardFD ( f , x , h )
    y = ( f(x+h) - f(x))/h
endfunction

// Returns the total error coming from Taylor's formula 
// truncation and rounding error.
// This relative error is exact if the rounding error 
// of f is %eps.
function relerr = predictedRelativeError ( x , h , f, fpp ) 
    r = %eps
    toterr = r * abs(f(x)) / h + h * abs(fpp(x)) / 2.0
    relerr = abs(toterr)/abs(fp(x))
    relerr = min(relerr,1)
endfunction

// Computes the relative error of the F.D. formula at point x 
// with step h.
function y = relativeerror ( f , fp , x , h )
    expected = fp ( x )
    computed = forwardFD ( f , x , h )
    y = abs ( computed - expected ) / abs( expected )
    y = min(y,1)
endfunction

// Plots the relative error in log scale for given f at point x.
// Considers the forward F.D. formula.
// The steps h are chosen in the range log(hlogrange)
function drawrelativeerror ( f , fp , fpp , x , mytitle , hlogrange )
    n = 1000;
    // Plot computed relative error
    logharray = linspace (hlogrange(1),hlogrange(2),n);
    for i = 1:n
        h = 10^(logharray(i));
        relerr = relativeerror ( f , fp , x , h );
        logearray ( i ) = log10 ( relerr );
    end
    plot ( logharray , logearray , "b-")
    xtitle(mytitle,"log10(h)","log10(RE)");
    // Plot predicted relative total error
    for i = 1:n
        h = 10^(logharray(i));
        relerr = predictedRelativeError ( x , h , f, fpp );
        logearray ( i ) = log10 ( relerr );
    end
    plot ( logharray , logearray , "r-")
    h = gce()
    h.children.thickness = 2
endfunction

// Computes the condition number of the function
function c = conditionnumber(x,f,fp)
    c = abs(x*fp(x)/f(x))
endfunction

// Compare the unscaled steps, the scaled step and the exact step 
// with the plot of the Relative Error.
function compareSteps(f , fp, fpp , x , mytitle , hlogrange)
    mprintf ( "x = %s\n", string(x))
    c = conditionnumber(x,f,fp)
    mprintf ( "C(x) = %s\n", string(c))
    drawrelativeerror ( f , fp , fpp , x , mytitle, hlogrange);
    //
    h1 = step_approximate ( x );
    mprintf ( "Step Unscaled = %s\n", string(h1))
    plot(log10(h1)*ones(2,1),[0;1],"r.-");
    //
    hS = step_approxScaled ( x );
    mprintf ( "Step Scaled = %s\n", string(hS))
    plot(log10(hS)*ones(2,1),[0;1],"k>-");
    //
    h2 = step_exact ( f , fpp , x );
    mprintf ( "Step Exact = %s\n", string(h2))
    plot(log10(h2)*ones(2,1),[0;1],"g*-");
    //
    legend(["Computed  LRE","Predicted LRE","Unscaled h","Scaled h","Exact h"]);
    h = gce()
    h.legend_location = "in_lower_right"
endfunction

///////////////////////////////////////////////////////////
//
// Applies the theory on the sqrt function
//

// The square root function
function y = mysqrt ( x )
    y = sqrt(x)
endfunction

// The derivative of the square root function.
function y = mydsqrt ( x )
    y = 0.5 * x^(-0.5)
endfunction

// The second derivative of the square root function.
function y = myddsqrt ( x )
    y = -0.25 * x^(-1.5)
endfunction


// Try in x = 1
// Plot the Relative Error
scf();
x = 1.0;
mytitle = "Numerical derivative of sqrt in x=1.0";
drawrelativeerror ( mysqrt , mydsqrt , myddsqrt, x , mytitle , [-16,0] );
legend(["Computed","Predicted"]);
h1 = step_approximate ( )
h2 = step_exact ( mysqrt , myddsqrt , x )


// Try in x = 1
// Plot the Relative Error + step
// All steps are ok.
x = 1.0;
scf();
mytitle = "Numerical derivative of sqrt in x=1.0";
compareSteps(mysqrt , mydsqrt, myddsqrt , x , mytitle , [-16,0]);


// Try in x = 1.e10
// Plot the Relative Error + step
// The unscaled step is very bad (too small).
scf();
x = 1.e10;
mytitle = "Numerical derivative of sqrt in x=1.e10";
compareSteps(mysqrt , mydsqrt, myddsqrt , x , mytitle , [-10,15]);

// Try in x = 1.e-10
// Plot the Relative Error + step
// The unscaled step is very bad (too large).
scf();
x = 1.e-10;
mytitle = "Numerical derivative of sqrt in x=1.e-10";
compareSteps(mysqrt , mydsqrt, myddsqrt , x , mytitle , [-25,-5]);

///////////////////////////////////////////////////////////
//
// Applies the theory on the power^20 function
//
// Nothing special
//

if ( %f ) then

    // The power^20 function
    function y = mypower20 ( x )
        y = x^20
    endfunction

    // The derivative of the power20 function.
    function y = mypower20p ( x )
        y = 20*x^19
    endfunction

    // The second derivative of the power20 function.
    function y = mypower20pp ( x )
        y = 20*19*x^18
    endfunction



    // Try in x = 1.e10
    // Plot the Relative Error + step
    // The unscaled step is very bad.
    x = 1.e10;
    scf();
    mytitle = "Numerical derivative of x^20 in x=1.e10";
    compareSteps(mypower20 , mypower20p, mypower20pp , x , mytitle , [-10,15]);


    // Try in x = 1.e-10
    // Plot the Relative Error + step
    // The unscaled step is very bad.
    x = 1.e-10;
    scf();
    mytitle = "Numerical derivative of x^20 in x=1.e-10";
    compareSteps(mypower20 , mypower20p, mypower20pp , x , mytitle , [-30,0]);

end

///////////////////////////////////////////////////////////
//
// Applies the theory on the exp function
//

// The exp function
function y = myexp ( x )
    y = exp(x)
endfunction

if ( %f ) then

    // Try in x = 1
    // Plot the Relative Error + step
    x = 1.0;
    scf();
    mytitle = "Numerical derivative of exp(x) in x=1.0";
    compareSteps(myexp , myexp, myexp , x , mytitle , [-16,0]);

end

// Try in x = 1.e-10
// Plot the Relative Error + step
// The scaled step is very bad.
// The cause is the scaling by abs(x), which has 
// no sense in this case.
x = 1.e-10;
scf();
mytitle = "Numerical derivative of exp(x) in x=1.e-10";
compareSteps(myexp , myexp, myexp , x , mytitle , [-20,5]);

///////////////////////////////////////////////////////////
//
// Applies the theory on the exp(a*x) function
//

// The exp function
function y = myexpA ( x )
    global MYA
    a = MYA
    y = exp(a * x)
endfunction

function y = myexpAp ( x )
    global MYA
    a = MYA
    y = a*exp(a * x)
endfunction

function y = myexpApp ( x )
    global MYA
    a = MYA
    y = a^2*exp(a * x)
endfunction


// In this case:
// * the unscaled step is much too large,
// * the scaled step is much too small.
global MYA
MYA = 1.e10
x = 1.e-20;
scf();
mytitle = "Numerical derivative of exp(x*10^10) in x=1.e-10";
compareSteps(myexpA , myexpAp, myexpApp , x , mytitle , [-30,-5]);

