// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Consider the forward formula to compute the first derivative
// of a scalar function of one parameter.
// Compare the relative error, depending on the step.
// Compare:
// * x = 1
// * x = 10
// * x = 100
//
// For f(x)=sqrt(x), we see that the optimum h depends on x.
// For f(x)=exp(x), we see that the optimum h 
// * does not depend on x, for abs(x)<=1
// * does depend on x, for abs(x)>=1
//

// Computes the approximate step for the forward F.D. formula.
// Uses the unscaled step.
function y = step_approxUnscaled ( x )
    y = sqrt(%eps)
endfunction


// Computes the first derivative with a forward F.D.
function y = forwardFD ( f , x , h )
    y = ( f(x+h) - f(x))/h
endfunction



// Computes the relative error of the F.D. formula at point x 
// with step h.
function y = relativeerror ( f , fp , x , h , fpapprox)
    expected = fp ( x )
    y = abs ( fpapprox - expected ) / abs( expected )
    y = min(y,1)
endfunction

// Plots the relative error in log scale for given f at point x.
// Considers the forward F.D. formulas.
// The steps h are chosen in the range log(hlogrange)
function drawrelativeerror ( f , fp , x , hlogrange , plottag )
    n = 1000;
    //
    // Plot computed relative error of forward F.D.
    logharray = linspace (hlogrange(1),hlogrange(2),n);
    for i = 1:n
        h = 10^(logharray(i));
        //
        fpapprox = forwardFD ( f , x , h )
        relerr = relativeerror ( f , fp , x , h , fpapprox );
        logearrayForward ( i ) = log10 ( relerr );
    end
    plot ( logharray , logearrayForward , plottag)
endfunction

///////////////////////////////////////////////////////////
//
// Applies this on the sqrt function
//

// The square root function
function y = mysqrt ( x )
    y = sqrt(x)
endfunction

// The derivative of the square root function.
function y = mydsqrt ( x )
    y = 0.5 * x^(-0.5)
endfunction


// Try in x = 1
// Plot the Relative Error
scf();
//
x = 1.0;
drawrelativeerror ( mysqrt , mydsqrt , x , [-16,0] , "b-");
//
x = 100.0;
drawrelativeerror ( mysqrt , mydsqrt , x , [-16,0] , "r--");
//
x = 10000.0;
drawrelativeerror ( mysqrt , mydsqrt , x , [-16,0] , "g-.");
//
xtitle("Numerical derivative of sqrt","log10(h)","log10(RE)")
legend(["x=1","x=10^2","x=10^4"])

///////////////////////////////////////////////////////////
//
// Applies this on the exp function
//

// The exp function
function y = myexp ( x )
    y = exp(x)
endfunction


// Plot the Relative Error for abs(x)<=1
scf();
//
x = 1.0;
drawrelativeerror ( myexp , myexp , x , [-16,0] , "b-");
//
x = 1.e-5;
drawrelativeerror ( myexp , myexp , x , [-16,0] , "r--");
//
x = 1.e-10;
drawrelativeerror ( myexp , myexp , x , [-16,0] , "g-.");
//
xtitle("Numerical derivative of exp","log10(h)","log10(RE)")
legend(["x=1","x=10^-5","x=10^-10"]);

// Plot the Relative Error for abs(x)>=1
scf();
//
x = 1.0;
drawrelativeerror ( myexp , myexp , x , [-16,0] , "b-");
//
x = 100;
drawrelativeerror ( myexp , myexp , x , [-16,0] , "r--");
//
xtitle("Numerical derivative of exp","log10(h)","log10(RE)")
legend(["x=1","x=100"]);
