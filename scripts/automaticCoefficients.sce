// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// Source:
// Derivative Approximation by Fincte Differences
// David Eberly, Geometric Tools, LLC, 1998-2008

// A finite difference (f.d.) formula can be defined by its 
// associated template. 
// The template is the set of coefficients Ci which appears 
// in the f.d. formula, which is written in the form:
//
// h^d/d! F^(d)(x) + O(h^{d+p}) = sum_{i=imin}^{imax) Ci F(x+ih)
//
// In other words, the f.d. formula of order p for F^(d) is:
//
//  F^(d)(x) = d!/h^d sum_{i=imin}^{imax) Ci F(x+ih) + O(h^{d+p})
//
// For example, the forward f.d. formula:
//
// F' = (F(x+h) - F(x))/h + O(h^1)
//
// is associated with the degree d=1 and order p=1.
// The template is defined by C0= -1, C1=1.
//
// For example, the centered f.d. formula:
//
// F' = (F(x+h) - F(x-h))/h + O(h^2)
//
// is associated with the degree d=1 and order p=2.
// The template is defined by C-1=-1, C0= 0, C1=1.
//

// The goal of the functions is to compute the template.

// Computes the indices of a fincte difference formula.
// Arguments
// d : the f.d. formula must approximate the d-th derivative of F, d>=1
// p : the order of the f.d. formula, p>0
// imin : the mincmum index of the template
// imax : the maximum index of the template
function [imin,imax] = derivativeIndices(d,p,form)
  select form
  case "forward"
    if ( p > 1 & modulo(p,2)==1 ) then
      error(msprintf("The order p must be even."))
    end
    imin = 0
    imax = d+p-1
  case "backward"
    if ( p > 1 & modulo(p,2)==1 ) then
      error(msprintf("The order p must be even."))
    end
    imin = -(d+p-1)
    imax = 0
  case "centered"
    if ( modulo(p,2)==1 ) then
      error(msprintf("The order p must be even."))
    end
    imax = floor((d+p-1)/2)
    imin = -imax
  else
    error(msprintf("Unknown form %s",form))
  end
endfunction

// Table p. 4
[imin,imax] = derivativeIndices(1,1,"forward") // 0, 1
[imin,imax] = derivativeIndices(1,1,"backward") // -1, 0
[imin,imax] = derivativeIndices(1,2,"centered") // -1, 1
//
[imin,imax] = derivativeIndices(1,2,"forward") // 0, 2
[imin,imax] = derivativeIndices(1,2,"backward") // -2, 0
[imin,imax] = derivativeIndices(1,4,"centered") // -2, 2
//
[imin,imax] = derivativeIndices(2,1,"forward") // 0, 2
[imin,imax] = derivativeIndices(2,2,"centered") // -1, 1
[imin,imax] = derivativeIndices(2,4,"centered") // -2, 2
//
// Impossible case
[imin,imax] = derivativeIndices(3,1,"centered")

mprintf("d, p, imin, imax, nc\n");
for d = [1 2]
for p = 2:2:9
  [imin,imax] = derivativeIndices(d,p,"centered");
  mprintf("%2d %2d %2d %2d %2d\n",...
     d, p,imin,imax, imax-imin+1);
end
end

// Computes the linear system which is associated with the problem.
function A = derivativeMatrixNaive(d,p,form)
  [imin,imax] = derivativeIndices(d,p,form)
  indices = imin:imax
  nc=size(indices,"*")
  A = zeros(nc,nc)
  for irow = 1 : nc
    n = irow-1
    for jcol = 1 : nc
      i = indices(jcol)
      A(irow,jcol) = i^n
    end
  end
endfunction

function A = derivativeMatrix(d,p,form)
  [imin,imax] = derivativeIndices(d,p,form)
  indices = imin:imax
  nc=size(indices,"*")
  A = zeros(nc,nc)
  x = indices(ones(nc,1),:)
  y = ((1:nc)-1)'
  z = y(:,ones(nc,1))
  A = x.^z
endfunction

// Example 1
// Approximate F^(3) with forward f.d. and error O(h):
// d=3, p=1
[imin,imax] = derivativeIndices(3,1,"forward") 
A = derivativeMatrix(3,1,"forward")
Aexpected = [
1 1 1 1
0 1 2 3
0 1 4 9
0 1 8 27
]

// Example 1
// Approximate F^(3) with centered f.d. and error O(h^2):
// d=3, p=2
[imin,imax] = derivativeIndices(3,2,"centered") 
A = derivativeMatrix(3,2,"centered")
Aexpected = [
1 1 1 1 1
-2 -1 0 1 2 
4 1 0 1 4
-8 -1 0 1 8
16 1 0 1 16
]

// Approximate F^(3) with centered f.d. and error O(h):
// d=3, p=1
[imin,imax] = derivativeIndices(3,1,"centered") 
A = derivativeMatrix(3,1,"centered")

// Approximate F^(2) with centered f.d. and error O(h^2):
// d=2, p=2
[imin,imax] = derivativeIndices(2,2,"centered") 
A = derivativeMatrix(2,2,"centered")
E = [
1 1 1
-1 0 1
1 0 1
]

// Approximate F^(2) with order 4
A = derivativeMatrix(2,4,"centered")

// Solves the linear system.
function C = derivativeTemplate(d,p,form)
  A = derivativeMatrix(d,p,form)
  nc=size(A,"r")
  b = zeros(nc,1)
  b(d+1) = 1
  C = A\b
endfunction

// Example 1
// Approximate F^(3) with forward f.d. and error O(h):
// d=3, p=1
C = derivativeTemplate(3,1,"forward")
Cexpected = [-1;3;-3;1]/6
//
// Example 1
// Approximate F^(3) with centered f.d. and error O(h^2):
// d=3, p=2
C = derivativeTemplate(3,2,"centered")
Cexpected = [-1;2;0;-2;1]/12

// Approximate F^(2) with centered f.d. and error O(h^2):
// d=2, p=2
C = derivativeTemplate(2,2,"centered")
Cexpected = [1;-2;1]/2

// Approximate F^(2) with centered f.d. and O(h^4)
C = derivativeTemplate(2,4,"centered")

d = 2
p = 4
form = "centered"
  A = derivativeMatrix(d,p,form)
  nc=size(A,"r")
  b = zeros(nc,1)
  b(d+1) = 1
  C = A\b
C*24

// Reproduce the table at:
// http://en.wikipedia.org/wiki/Fincte_difference_coefficient
// 1 2 				-1/2	0	1/2
// 1 4 			1/12	-2/3	0	2/3	-1/12
// 1 6 		-1/60	3/20	-3/4	0	3/4	-3/20	1/60
// 1 8 	1/280	-4/105	1/5	-4/5	0	4/5	-1/5	4/105	-1/280
// 2 2 				1	-2	1
// 2 4 			-1/12	4/3	-5/2	4/3	-1/12
// 2 6 		1/90	-3/20	3/2	-49/18	3/2	-3/20	1/90
// 2 8 	-1/560	8/315	-1/5	8/5	-205/72	8/5	-1/5	8/315	-1/560
// 3 2 				-1/2	1	0	-1	1/2
// 3 4 			1/8	-1	13/8	0	-13/8	1	-1/8
// 3 6 		-7/240	3/10	-169/120	61/30	0	-61/30	169/120	-3/10	7/240
// 3 8 	41/6048	-1261/15120	541/1120	-4369/2520	1669/720	0	-1669/720	4369/2520	-541/1120	1261/15120	-41/6048
// 4 2 				1	-4	6	-4	1
// 4 4 			-1/6	2	-13/2	28/3	-13/2	2	-1/6
// 4 6 		7/240	-2/5	169/60	-122/15	91/8	-122/15	169/60	-2/5	7/240
// 4 8 	-41/7560	1261/15120	-541/840	4369/1260	-1669/180	1529/120	-1669/180	4369/1260	-541/840	1261/15120	-41/7560

mprintf("Derivative	Accuracy\n");
CTable = [];
HTable = [];
k = 0;
icenter = 5;
for d = 1:4
for p = 2:2:6
  C = factorial(d)*derivativeTemplate(d,p,"centered");
  nc = size(C,"*");
  k=k+1;
  jmin = icenter-(nc-1)/2;
  jmax = icenter+(nc-1)/2;
  CTable(k,jmin:jmax) = C';
  HTable(k,1:2) = [d p];
end
end
disp([HTable CTable])
[nu,de]=rat(CTable,1000*%eps);
CTableStr = "";
for i = 1 : size(CTable,"r")
  for j = 1 : size(CTable,"c")
    if (nu(i,j)==0) then
      CTableStr(i,j) = "0";
    elseif (de(i,j)==1) then
      CTableStr(i,j) = string(nu(i,j));
    else
      CTableStr(i,j) = string(nu(i,j))+"/"+string(de(i,j));
    end
  end
end
HTableStr = string(HTable);
disp([HTableStr CTableStr])
Table = [HTableStr CTableStr];
for k = 1 : size(Table,"r")
  mprintf("%s\\\\\n",strcat(Table(k,:),"&"))
end


// Evaluates the f.d. formula, given the degree, order and formula
function y = derivativeTemplateApply(f,x,d,p,form,h)
  n = size(x,"*")
  [imin,imax] = derivativeIndices(d,p,form)
  C = derivativeTemplate(d,p,form)
  t = size(C,"*")
  indices = imin:imax
  for i = 1 : t
      if (C(i)==0) then
          z(i)=0
      else
          z(i) = C(i)*f(x+indices(i)*h)
      end
  end
  y = sum(z)*factorial(d)/h^d
endfunction

// Apply template on f
function f = quadf ( x )
    f = x^2 + exp(x)
endfunction

function J = quadJ ( x )
    J = 2 * x + exp(x)
endfunction
x = 3;
h = 1.e-1;
J = quadJ(x)
//
// Order 4, forward
d = 1;
p = 4;
form = "forward";
Japprox = derivativeTemplateApply(quadf,x,d,p,form,h)
d = assert_computedigits(J,Japprox)
//
// Order 4, centered
d = 1;
p = 4;
form = "centered";
Japprox = derivativeTemplateApply(quadf,x,d,p,form,h)
d = assert_computedigits(J,Japprox)

// Analyze condition number depending on order
for p = 1:16
  A = derivativeMatrix(1,p,"centered"); 
  c = ceil(log10(cond(A)));
  mprintf("d=1, p=%d, condition=10^%d\n",p,c);
end

// TODO : compute approximate optimal step !
