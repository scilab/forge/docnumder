// Copyright (C) 2017 - Michael Baudin
// Adapted from the Matlab implementation at :
// Bengt Fornberg (2011) Finite difference method. Scholarpedia, 6(10):9685.
function c = allweights(z,x,m)
    // Calculates FD weights. The parameters are:
    //  z   location where approximations are to be accurate,
    //  x   vector with x-coordinates for grid points,
    //  m   highest derivative that we want to find weights for
    //  c   array size m+1,lentgh(x) containing (as output) in 
    //      successive rows the weights for derivatives 0,1,...,m.

    n=length(x); 
    c=zeros(m+1,n); 
    c1=1; 
    c4=x(1)-z; 
    c(1,1)=1;
    for i=2:n
        mn=min(i,m+1); 
        c2=1; 
        c5=c4; 
        c4=x(i)-z;
        for j=1:i-1
            c3=x(i)-x(j);  
            c2=c2*c3;
            if j==i-1 
                c(2:mn,i)=c1*((1:mn-1)'.*c(1:mn-1,i-1)-c5*c(2:mn,i-1))/c2;
                c(1,i)=-c1*c5*c(1,i-1)/c2;
            end
            c(2:mn,j)=(c4*c(2:mn,j)-(1:mn-1)'.*c(1:mn-1,j))/c3;
            c(1,j)=c4*c(1,j)/c3;
        end
        c1=c2;
    end
endfunction

function w = weights(x)
    z = 0
    m = length(x)
    c = allweights(z,x,m)
    w = c(m,:)
endfunction

x=-2:2
w=allweights(0,x,6)
disp(w)
w=weights(x)
disp(w)

n=6
// Regular grid
x=linspace(-1,1,n)
w=weights(x)
sum(abs(w),"c")
// Chebyshev nodes
[x,w]=chebyshev_quadrature(n);
disp(x')
w=weights(x)
sum(abs(w),"c")
// Legendre nodes
[x,w]=legendre_quadrature(n);
disp(x')
w=weights(x)
sum(abs(w),"c")


// Search for optimal points
// We minimize the sum of the absolute 
// values : this is the condition number of 
// the sum
function f=weightsum(x)
    w = weights(x)
    f = sum(abs(w))
endfunction

function [f, g, ind]=weightsumCost(x, ind)
    f = weightsum (x);
    g = numderivative (weightsum, x);
    if (ind == 1) then
        mprintf("f(x) = %s, |g(x)|=%s\n", string(f), string(norm(g)))
    end
endfunction

n=5
x0 = linspace(-1,1,n)
bsup = ones(1,n)
binf = -bsup
[fopt, xopt] = optim (weightsumCost, "b",binf,bsup, x0, imp=-1)

