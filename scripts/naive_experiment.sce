// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Compute the derivative with a naive method and compare with 
// the derivative function.
function fp = myfprime(f,x,h)
  fp = (f(x+h) - f(x))/h;
endfunction
function y = myfunction (x)
  y = x*x;
endfunction
x = 1.0;
fpref = derivative(myfunction,x);
fpexact = 2.;
e = abs(fpref-fpexact)/fpexact;
mprintf("Scilab f''=%e, error=%e\n", fpref,e);
h = 1.e-16;
fp = myfprime(myfunction,x,h);
e = abs(fp-fpexact)/fpexact;
mprintf("Naive f''=%e, error=%e\n", fp,e);

// Perform a loop with decreasing step sizes
x = 1.0;
fpexact = 2.;
fpref = derivative(myfunction,x,order=1);
e = abs(fpref-fpexact)/fpexact;
mprintf("Scilab f''=%e, error=%e\n", fpref,e);
h = 1.0;
for i=1:20
  h=h/10.0;
  fp = myfprime(myfunction,x,h);
  e = abs(fp-fpexact)/fpexact;
  mprintf("Naive f''=%e, h=%e, error=%e\n", fp,h,e);
end

