// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// A collection of formula for the Jacobian
function J = derivativeJacobian(f,x,h,form)
    n = size(x,"*")
    D = diag(h)
    for i = 1 : n
        d = D(:,i)
        select form
        case "forward2points" then   // Order 1
            J(i) = (f(x+d)-f(x))/h(i)
        case "backward2points" then    // Order 1
            J(i) = (f(x)-f(x-d))/h(i)
        case "centered2points" then    // Order 2
            J(i) = (f(x+d)-f(x-d))/(2*h(i))
        case "doubleforward3points" then    // Order 2
            J(i) = (-f(x+2*d)+4*f(x+d)-3*f(x))/(2*h(i))
        case "doublebackward3points" then    // Order 2
            J(i) = (f(x-2*d)-4*f(x-d)+3*f(x))/(2*h(i))
        case "centered4points" then    // Order 4
            J(i) = (-f(x+2*d) + 8*f(x+d)...
            -8*f(x-d)+f(x-2*d))/(12*h(i))
        else
            error(msprintf("Unknown formula %s",form))
        end
    end
endfunction

// The approximate unscaled steps for the Jacobian
function h = derivativeJacobianStep(form)
    select form
    case "forward2points" then    // Order 1
        h=%eps^(1/2)
    case "backward2points" then    // Order 1
        h=%eps^(1/2)
    case "centered2points" then    // Order 2
        h=%eps^(1/3)
    case "doubleforward3points" then    // Order 2
        h=%eps^(1/3)
    case "doublebackward3points" then    // Order 2
        h=%eps^(1/3)
    case "centered4points" then    // Order 4
        h=%eps^(1/5)
    else
        error(msprintf("Unknown formula %s",form))
    end
endfunction

//
// Test with a quadratic
//

function f = quadf ( x )
    f = x(1)^2 + x(2)^2
endfunction

function J = quadJ ( x )
    J(1) = 2 * x(1)
    J(2) = 2 * x(2)
endfunction

function H = quadH ( x )
    H(1,1) = 2
    H(1,2) = 0
    H(2,1) = 0
    H(2,2) = 2
endfunction

formMatrix = [
"forward2points"        // Order 1
"backward2points"       // Order 1
"centered2points"       // Order 2
"doubleforward3points"  // Order 2
"doublebackward3points" // Order 2
"centered4points"       // Order 4
];

x=[1;2];
J = quadJ ( x );
mprintf("Jexact\n");
disp(J);

form = "forward2points";
h = derivativeJacobianStep(form);
h = h*ones(2,1);
Japprox = derivativeJacobian(quadf,x,h,form);
mprintf("Japprox\n");
disp(Japprox);

for form = formMatrix'
    h = derivativeJacobianStep(form);
    h = h*ones(2,1);
    Japprox = derivativeJacobian(quadf,x,h,form);
    d = assert_computedigits(J,Japprox(:));
    mprintf("%s: Japprox = [%s,%s], d=%d\n",..
    form, string(Japprox(1)),string(Japprox(2)),d);
end
//
// Test with a sin-exp
//

function f = sinExpf ( x )
    f = exp(x(1)) + sin(x(2))
endfunction

function J = sinExpJ ( x )
    J(1) = exp(x(1))
    J(2) = cos(x(2))
endfunction

formMatrix = [
"forward2points"        // Order 1
"backward2points"       // Order 1
"centered2points"       // Order 2
"doubleforward3points"  // Order 2
"doublebackward3points" // Order 2
"centered4points"       // Order 4
];

x=[1;2];
J = sinExpJ ( x );
mprintf("Jexact = [%s,%s]\n",string(J(1)),string(J(2)));
for form = formMatrix'
    h = derivativeJacobianStep(form);
    h = h*ones(2,1);
    Japprox = derivativeJacobian(sinExpf,x,h,form);
    d = assert_computedigits(J,Japprox(:));
    mprintf("%s: Japprox = [%s,%s], d=%d\n",..
    form, string(Japprox(1)),string(Japprox(2)),d);
end

