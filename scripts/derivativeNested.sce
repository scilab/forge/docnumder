// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Evaluates f.
function y = derivativeEvalf(__derEvalf__,x)
    if ( typeof(__derEvalf__)=="function" ) then
        y = __derEvalf__(x)
    elseif ( typeof(__derEvalf__)=="list" ) then
        __f_fun__ = __derEvalf__(1)
        y = __f_fun__(x,__derEvalf__(2:$))
    else
        error(msprintf("Unknown function type %s",typeof(f)))
    end
endfunction

// A collection of formulas for the Jacobian
function J = derivativeJacobian(__derJacf__,x,h,form)
    n = size(x,"*")
    D = diag(h)
    for i = 1 : n
        d = D(:,i)
        select form
        case "forward2points" then   // Order 1
            y(:,1) = -derivativeEvalf(__derJacf__,x)
            y(:,2) = derivativeEvalf(__derJacf__,x+d)
        case "backward2points" then    // Order 1
            y(:,1) = derivativeEvalf(__derJacf__,x)
            y(:,2) = -derivativeEvalf(__derJacf__,x-d)
        case "centered2points" then    // Order 2
            y(:,1) = 1/2*derivativeEvalf(__derJacf__,x+d)
            y(:,2) = -1/2*derivativeEvalf(__derJacf__,x-d)
        case "doubleforward3points" then    // Order 2
            y(:,1) = -3/2*derivativeEvalf(__derJacf__,x)
            y(:,2) = 2*derivativeEvalf(__derJacf__,x+d)
            y(:,3) = -1/2*derivativeEvalf(__derJacf__,x+2*d)
        case "doublebackward3points" then    // Order 2
            y(:,1) = 3/2*derivativeEvalf(__derJacf__,x)
            y(:,2) = -2*derivativeEvalf(__derJacf__,x-d)
            y(:,3) = 1/2*derivativeEvalf(__derJacf__,x-2*d)
        case "centered4points" then    // Order 4
            y(:,1) = -1/12*derivativeEvalf(__derJacf__,x+2*d)
            y(:,2) = 2/3*derivativeEvalf(__derJacf__,x+d)
            y(:,3) = -2/3*derivativeEvalf(__derJacf__,x-d)
            y(:,4) = 1/12*derivativeEvalf(__derJacf__,x-2*d)
        else
            error(msprintf("Unknown formula %s",form))
        end
        J(:,i) = sum(y,"c")/h(i)
    end
endfunction

// The approximate unscaled steps for the Jacobian
function h = derivativeJacobianStep(form)
    select form
    case "forward2points" then    // Order 1
        h=%eps^(1/2)
    case "backward2points" then    // Order 1
        h=%eps^(1/2)
    case "centered2points" then    // Order 2
        h=%eps^(1/3)
    case "doubleforward3points" then    // Order 2
        h=%eps^(1/3)
    case "doublebackward3points" then    // Order 2
        h=%eps^(1/3)
    case "centered4points" then    // Order 4
        h=%eps^(1/5)
    else
        error(msprintf("Unknown formula %s",form))
    end
endfunction

// The approximate unscaled steps for the Hessian
function h = derivativeHessianStep(form)
    select form
    case "forward2points" then    // Order 1
        h=%eps^(1/3)
    case "backward2points" then    // Order 1
        h=%eps^(1/3)
    case "centered2points" then    // Order 2
        h=%eps^(1/4)
    case "doubleforward3points" then    // Order 2
        h=%eps^(1/4)
    case "doublebackward3points" then    // Order 2
        h=%eps^(1/4)
    case "centered4points" then    // Order 4
        h=%eps^(1/6)
    else
        error(msprintf("Unknown formula %s",form))
    end
endfunction

//
// Test with a vectorial function
//

function y = quadf ( x )
    f1 = x(1)^2 + x(2)^3 + x(3)^4
    f2 = exp(x(1)) + 2*sin(x(2)) + 3*cos(x(3))
    y = [f1;f2]
endfunction

function J = quadJ ( x )
    J1(1) = 2 * x(1)
    J1(2) = 3 * x(2)^2
    J1(3) = 4 * x(3)^3
    //
    J2(1) = exp(x(1))
    J2(2) = 2*cos(x(2))
    J2(3) = -3*sin(x(3))
    //
    J = [J1';J2']
endfunction

function H = quadH ( x )
    H1 = [
    2	0		0
    0	6*x(2)	0
    0	0		12*x(3)^2
    ]
    //
    H2 = [
    exp(x(1)) 0 0
    0 -2*sin(x(2)) 0
    0 0 -3*cos(x(3))
    ]
    //
    H = [H1;H2]
endfunction


x=[1;2;3];
J = quadJ ( x );
mprintf("Jexact = \n")
disp(J);

x=[1;2;3];
form = "forward2points";
h = derivativeJacobianStep(form);
h = h*ones(3,1);
Japprox = derivativeJacobian(quadf,x,h,form)



for form = formMatrix'
    h = derivativeJacobianStep(form);
    h = h*ones(3,1);
    Japprox = derivativeJacobian(quadf,x,h,form);
    d = assert_computedigits(J,Japprox);
    mprintf("%s: Digits=%d\n",form,min(d));
end

//
// Compose derivatives
// Compute H, by differentiating some function.
//

function J = derivativeFunctionJ(x,f,h,form)
    J = derivativeJacobian(f,x,h,form)
    J = J'
    J = J(:)
endfunction

mprintf("\nComputing Hessian...\n")
form = "forward2points";
x=[1;2;3];
H = quadH ( x );
mprintf("derivativeFunctionJ = \n")
h = derivativeHessianStep(form);
h = h*ones(3,1);
Japprox = derivativeFunctionJ(x,quadf,h,form);
disp(Japprox)

x=[1;2;3];
form = "forward2points";
h = derivativeHessianStep(form);
h = h*ones(3,1);
funlist = list(derivativeFunctionJ,quadf,h,form);
Happrox = derivativeJacobian(funlist,x,h,form)


formMatrix = [
"forward2points"        // Order 1
"backward2points"       // Order 1
"centered2points"       // Order 2
"doubleforward3points"  // Order 2
"doublebackward3points" // Order 2
"centered4points"       // Order 4
];

for form = formMatrix'
    h = derivativeHessianStep(form);
    h = h*ones(3,1);
    funlist = list(derivativeFunctionJ,quadf,h,form);
    Happrox = derivativeJacobian(funlist,x,h,form);
    d = assert_computedigits(H,Happrox);
    mprintf("%s: Digits=[%d,%d]\n",form,min(d),max(d));
end

